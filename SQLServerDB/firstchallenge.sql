/*create database firstchallenge;*/

use firstchallenge;

create table usuario(
	Id int IDENTITY(1,1) PRIMARY KEY,
	NomeCompleto varchar(50) not null,
	Apelido varchar(20) not null,
	Email varchar(50) not null,
	Telefone varchar(50) not null,
);

create table cliente(
	Id int IDENTITY(1,1) PRIMARY KEY,
	RazaoSocial varchar(50) not null,
	NomeFantasia varchar(30) not null,
	Cnpj varchar(20) not null,
	Logradouro varchar(50) not null,	
	Numero int not null,
	Bairro varchar(50) not null,
	Complemento varchar(100),
	Municipio varchar(30) not null,
	CEP int not null
);